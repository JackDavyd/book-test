<?php

namespace app\models\author;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AuthorSearch represents the model behind the search form of `app\models\author\Author`.
 */
class AuthorSearch extends Author
{
    public $book_count = null;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'book_count'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Author::find()->joinWith(['books'])->select('au.*, count(book_author.author_id) as book_count')->alias('au')->groupBy(['au.id']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['attributes'=>['book_count', 'name', 'id'], 'defaultOrder' => ['id' => SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'author.id' => $this->id,
        ]);
        $query->andFilterHaving([
            'book_count' => $this->book_count,
        ]);

        return $dataProvider;
    }
}
