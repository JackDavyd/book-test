<?php

namespace app\models\book;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\book\Book;

/**
 * BookSearch represents the model behind the search form of `app\models\book\Book`.
 */
class BookSearch extends Book
{
    public $authors_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['authors_name'], 'string'],
            [['title', 'authors_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find()->joinWith(['authors']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['attributes'=>['title', 'authors_name'=>
                [
                    'asc' => ['author.id' => SORT_ASC],
                    'desc' => ['author.id' => SORT_DESC],
                    'default' => ['author.id' => SORT_ASC]
                ],
             'id'], 'defaultOrder' => ['id' => SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'book.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'book.title', $this->title]);
        $query->andFilterWhere(['like', 'author.name', $this->authors_name]);

        return $dataProvider;
    }
}
