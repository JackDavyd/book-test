<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Тестовое задание</h1>
    </div>

    <div class="body-content">
        <ol>
            <li>Реализовать сущности авторы и книги</li>
            <li>Реализовать административную часть
                <ol>
                    <li>CRUD операции для авторов и книг</li>
                    <li>вывести список книг с обязательным указанием имени автора в списке</li>
                    <li>вывести список авторов с указанием кол-ва книг</li>
                </ol>
            </li>
            <li>Реализовать публичную часть сайта с отображение авторов и их книг (простой вывод списка на странице)
            </li>
        </ol>

    </div>
</div>
