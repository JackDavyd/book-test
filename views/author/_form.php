<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\book\Book;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\author\Author */
/* @var $form yii\widgets\ActiveForm */
$items = ArrayHelper::map(Book::find()->all(),'id', 'title');
$params = [
    'prompt' => 'Выберите книги...',
    'multiple' => true,
];
?>

<div class="author-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?=$form->field($model, 'books')->dropDownList($items,$params); ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
