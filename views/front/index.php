<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\author\AuthorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отображение авторов и их книг ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',

    ]); ?>


</div>
