<?php
/* @var $model yii\data\ActiveDataProvider */
?>
<div class="card" style="width: 18rem;">

    <div class="card-body">
        <h5 class="card-title"><?= $model->name ?></h5>
        <?php if ($model->books) : ?>
        <h6 class="card-title">Книги</h6>
        <ul>
            <?php
            foreach ($model->books as $book) : ?>
            <li><?= $book->title ?>
                <?php endforeach; ?>
        </ul>
        <?php else: ?>
        <p>Книг нет</p>
        <?php endif; ?>
    </div>
</div>
