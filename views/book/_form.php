<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\author\Author;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\book\Book */
/* @var $form yii\widgets\ActiveForm */
$items = ArrayHelper::map(Author::find()->all(),'id', 'name');
$params = [
    'prompt' => 'Выберите авторов...',
    'multiple' => true,
];
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?=$form->field($model, 'authors')->dropDownList($items,$params); ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
