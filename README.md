
REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 7.4.0.


INSTALLATION
------------

### Install via Composer

Выполнить в корневой директории

~~~
$ git clone https://JackDavyd@bitbucket.org/JackDavyd/book-test.git .
$ composer install
~~~

настроить соединение с БД, настроить сервер на корневую директорию web (как обычно для yii) и выполнить

~~~
$ php yii migrate
~~~

В архиве book-test.zip собранное приложение. Распаковать в корень должно завеститсь сразу.

